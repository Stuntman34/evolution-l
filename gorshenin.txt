{
    "_id": "580dfe093fe7e52724201509",
    "index": 1,
    "guid": "b366316b-3d13-4d5a-8bb9-25238f6b8f52",
    "isActive": false,
    "balance": "$2,138.80",
    "picture": "http://placehold.it/32x32",
    "age": 38,
    "eyeColor": "green",
    "name": "Jordan Branch",
    "gender": "male",
    "company": "GROK",
    "email": "jordanbranch@grok.com",
    "phone": "+1 (814) 505-2981",
    "address": "778 Cherry Street, Hiseville, Marshall Islands, 7390",
    "about": "Ex id tempor enim aute laboris Lorem eiusmod tempor dolore reprehenderit commodo sit mollit aute. Laboris deserunt tempor labore veniam. Dolor in do sint qui excepteur.\r\n",
    "registered": "2016-08-30T09:27:57 -03:00",
    "latitude": 88.894512,
    "longitude": -177.068667,
    "tags": [
      "consequat",
      "Lorem",
      "ad",
      "et",
      "anim",
      "et",
      "elit"
    ],
    "friends": [
      {
        "id": 0,
        "name": "Gilda Patel"
      },
      {
        "id": 1,
        "name": "Burnett Joseph"
      },
      {
        "id": 2,
        "name": "Petra Young"
      }
    ],
    "greeting": "Hello, Jordan Branch! You have 8 unread messages.",
    "favoriteFruit": "banana"
  }